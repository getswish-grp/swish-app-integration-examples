# Swish App Integration Examples
## Objective C
### Checking if the Swish app is installed

```objective-c
- (BOOL)isSwishAppInstalled {
    return [[UIApplication sharedApplication] canOpenURL: [NSURL URLWithString:@"swish://"]];
}
```

### Switching to the Swish app

```objective-c
- (void)openSwishAppWithToken:(NSString *)token {
    // character set is used to encode callback URL properly
    NSCharacterSet *notAllowedCharactersSet =
    [NSCharacterSet characterSetWithCharactersInString:@"!*'();:@&=+$,/?%#[]"];
    
    NSCharacterSet *allowedCharactersSet =
    [notAllowedCharactersSet invertedSet];
    
    NSString *callbackURLStr = @"merchant://";
    
    NSString *encodedCallbackURLStr =
    [callbackURLStr stringByAddingPercentEncodingWithAllowedCharacters:allowedCharactersSet];
    
    NSString *swishURLStr = [NSString stringWithFormat:@"swish://paymentrequest?token=%@&callbackurl=%@", token, encodedCallbackURLStr];
    
    NSURL *swishURL = [[NSURL alloc] initWithString: swishURLStr];
    
    if ([self isSwishAppInstalled]) {
        [[UIApplication sharedApplication] openURL:swishURL options:@{} completionHandler:^(BOOL success) {
            if (success) {
                // Success
            } else {
                // Error handling
            }
        }];
    } else {
        // Swish app is not installed
        // error handling
    }    
}
```

## Java
### Checking if the Swish app is installed

```java
public static boolean isSwishInstalled(Context context) {
    try {
        context.getPackageManager()
                .getPackageInfo("se.bankgirot.swish", 0);
        return true;
    } catch (PackageManager.NameNotFoundException e) {
        // Swish app is not installed
        return false;
    }
}
```

### Switching to the Swish app

```java
public static boolean openSwishWithToken(Context context, String token, String callBackUrl) {
    if ( token == null
            || token.length() == 0
            || callBackUrl == null
            || callBackUrl.length() == 0
            || context == null) {
        return false;
    }

    // Construct the uri
    // Note that appendQueryParameter takes care of uri encoding
    // the parameters
    Uri url = new Uri.Builder()
            .scheme("swish")
            .authority("paymentrequest")
            .appendQueryParameter("token", token)
            .appendQueryParameter("callbackurl", callBackUrl)
            .build();

    Intent intent = new Intent(Intent.ACTION_VIEW, url);
    intent.setPackage("se.bankgirot.swish");

    try {
        context.startActivity(intent);
    } catch (Exception e){
        // Unable to start Swish
        return false;
    }

    return true;
}
```
